---
title: Do We Still Have School Tomorrow?
author: Denshi
date: Sun, 30 Oct 2022 00:38:09 +0400
draft: false
---
![The planet Earth is in shambles, yet we humbly ask whether school's still on](/tomorrow.jpg)

I have this really vivid memory of when I was planning to take my GCSEs.
I remember being quite stressed about the whole ordeal,
not because I didn't have faith in my own abilities as a student
but because I was worried about any cataclysmic event that might happen
that would disrupt my exams.
I would be paranoid about something happening to me or someone I loved
throwing me down a downward spiral of horrible feelings.
Examples included relatives dying, me getting impaired in an accident,
or some kind of collective societal "breakdown" like a political crisis or even a war.

Now the reason lil' ol' me was that worried about that happening
was that the school had emphasized how not all students get to perform as they wish
once they actually get to their important exams.
We were given examples of how even some members of our faculty
had familiar or medical issues arise that prevented them from completing their GCSEs.
Funnily enough, by the time I actually got to my GCSEs,
they were all canceled and replaced with evidence-based, in-school evaluations.
I never actually got to sit any of the official tests,
meaning I was lucky enough to avoid the aforementioned stress,
safe in the knowledge that my teachers had collected enough evidence
to justify grades to the exam board.
**It's in that moment I realized how I'd been tricked into being paranoid.**

## Anti-Social Reactions to Disasters

Nowadays, whenever I hear students stress about unexpected occurrences during exam season,
I always think back to the meme image at the top of this article:
The Earth is in a state of pure disarray
and yet people's concerns are with the functioning of existing institutions
rather then the protection and well-being of themselves and their loved ones.
***Just have a moment to think about how disgusting it is
for a student to be seriously concerned about the effect
the death of a relative may have on their examinations.***

This is why I want to let all the young people (including myself) who read this blog
to please **let go of that mentality.**
There are many institutional things we find valuable in this world,
like school, private infrastructure like websites and services,
and just anything we associate with "everyday life".
I've seen variants of that Earth disaster meme referencing the development of video-games
or the maintenance of video game servers,
which makes sense considering how much of the youth regards online gaming
as a daily, re-occurring activity rather than a novelty or occasional special occurrence.
**In a way this is a form of brainwashing.
One so effective to the point where, instead of concerning themselves with the emotional issue at hand,
people are moreso interested in how it effects their daily lives.**

I really do regret worrying myself so much when I thought to exams,
and I can gladly say that I no longer have such an attitude to unexpected events
during periods of critical work.
Be content to **drop everything** at any moment if something seriously bad happens;
***sometimes it's good to just stop caring
if it means living the rest of your life knowing you were there for others.***

## Or in other words...

![I hope my family is OK.](/conclusion.png)

(This image was made by Eli Nelson Marsden. Thanks for the meme Eli!) 
