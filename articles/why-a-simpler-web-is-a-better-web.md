---
title: Why a Simpler Web is a Better Web
author: Denshi
date: Sat, 18 Sep 2021 09:32:27 +0400
---

Ever since I've gotten into self-hosting and making videos about it, 
I've heard people talk about ***Pleroma, ActivityPub,*** and other **decentralized social media-like protocols.**
It sounds excellent at first, creating new standards to re-create the centralized social media experience of Twitter, Facebook and other "walled gardens",
but after a while I realized that it's not quite for me.

## The problem with the Fediverse
The Fediverse is a simple concept: A few standardized protocols (ActivityPub, Diaspora, and the older OStatus) are used by individual servers to communicate information with each other,
to allow for one massive decentralized network.
**However, this system has some fundamental flaws.**

The biggest single flaw is that **servers can still ban you!**
This has happened before (just look at Mastodon) and it'll only keep happening unless people realize that any form of decentralized system that **still relies on a centralized host** (ie. the most popular Pleroma instance) isn't going to prevent censorship.

In the end, the real problem with the Fediverse is that it still requires the end-user to use a single web client, which can enact censorship.
You might think this can be mitigated by simply switching to other, more free-speech friendly websites,
but the most popular ActivityPub/Diaspora instances can just block that one, effectively cutting you off.

## Older standards are better!
A far better standard for social media communication is **RSS,**
which is what you're likely using right now to read this blog post.
Because RSS subscriptions are handled directly between the **individual user and server,**
it means that no intermediary server can censor them.
This is so much better than any Fediverse social media.

## "B-but MUH REPOSTING/LIKING/DISLIKING!!!"
**Social media is the devil.**
While I can understand wanting to keep up with your friends' blogs and art feeds,
I will never understand people's fixation with online "socializing" in this manner.
**You don't need to be constantly retweeting and liking posts to survive.**
Luckily, most people seem to agree with me on the incredibly detrimental effects of social media addiction,
which restores a lot of my faith in humanity.

Also, if you want to communicate with anyone in a decentralized manner,
you can always be using **XMPP** or **Matrix** to message them.
Both are effective at offering secure, decentralized communication for individual and group chats,
and I think we'll see far more adoption of both of them with time.

## Conclusion
At the end of the day, social media itself is **awfully structured.**
Even if the Fediverse was completely censorship-free, it'd probably just become like Reddit:
A hivemind powered by upvotes and downvotes in perfect balance so that Keanu Reeves and Big Chungus memes can reign supreme over what was meant to be a platform for proper discussion.
RSS feeds and personal websites are and will always remain the most direct,
simple and free way to communicate ideas and knowledge on the internet,
and I can only see this becoming more and more important as the Fediverse continues to gain traction.
