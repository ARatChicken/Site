---
title: Wowneria is Closing
author: Denshi 
date: Wed, 16 Jun 2021 19:52:30 +0400
---

## After a lot of consideration, I've decided to close down this pool in support of Wownero's new ["Junkie Jeff"](https://git.wownero.com/wownero/wownero/releases/tag/v0.10.0.0) fork.
**After the second and final block is confirmed and the final payments are through,
Wowneria will close to support the new solo-mining initiative catalyzed by the new Wownero fork.**

A cryptocurrency is only as good as its community makes it;
the increasing corporatization and centralization of cryptos like [Bitcoin](https://bitcoin.org) displays a clear threat to other, smaller projects.
If the **"people"** mining Bitcoin, Litecoin and other non-ASIC-resistant cryptos aren't individuals but massively-funded mining operations that fill literal warehouses of ASICs,
then in my eyes, these decentralized currencies have failed at their objectives.

[Monero](https://getmonero.org) is bold in its defense of **privacy,**
an everlasting yet increasingly overlooked human struggle in the modern age.
Just as it refreshingly improves upon the concept of Bitcoin for the end user,
Monero also improves the ecosystem for miners,
who now have a **CPU-centric algorithm** that anyone with a computer can use to contribute to the Monero network.

In a similar way, [Wownero](https://wownero.org) is bold in being both a supposed "meme coin" that maintains integrity by **refusing to be endorsed by "lame celebrities" and shills,**
but also offering **genuine technical improvements** and **notable differences** to Monero.
Take the new mining system which essentially eliminates the prospect of creating large, centralized pools mining Wownero.

So yes, Wonweria is closing after the final block is confirmed and everyone is paid.
