---
title: You're Free Now!
author: Denshi
date: Tue, 12 Oct 2021 20:01:10 +0400
---

**It's absolutely no secret that I love self-hosting.**
But something that has been bothering me for a while about the modern self-hosting world is people's obsessions with *over-complicating everything.*
At first I just thought I was stupid, and I didn't have the knowledge or **Reddit karma from my intellectual®® and quaint takes from r/atheism,**
but soon I realized that what a lot of people wanted to replicate with their homeserver or VPS was a *similar experience to existing, centralized services ran by tech monopolies.*

## Uhm, you do know you're free now, right?
The first thing I thought when I leaned more about self-hosting wasn't *"hm, how will I build my own Google?",*
it was more along the lines of *"how often will I actually have to use this thing?".*
Setting up alternatives to
[social media](https://pleroma.social/),
[web password managers](https://github.com/dani-garcia/vaultwarden),
[web cloud storage](https://nextcloud.com/),
[collaborative document editing](https://cryptpad.org/)
and other **Silicon Valley-esque products**
wasn't as interesting to me as exploring the completely new and fascinating world of
**setting up a personal website,**
[email server](https://github.com/LukeSmithxyz/emailwiz)
and [federated](https://matrix.org) chat [server.](https://xmpp.org)

While I still did try to use bloated web "apps" like Nextcloud,
I soon realized these were very overkill for any kind of cloud storage I needed and I simply stuck to using [rsync](https://rsync.samba.org/).
***Self-hosting isn't about replacing everything you already had with self-hosted versions, it's about changing the way one looks at their internet and bandwidth usage.***
Not to mention that software like Nextcloud has *essentially become a corporate product.*
Yes, it's still "free/libre software",
but it's so incredibly slow and painful to setup as an individual,
which is a very common characteristic of business/corporate software.

More to the point, when I say *"changing the way one looks at their internet and bandwidth usage",*
I mean coming to the realization that so many of the things people tend to use the web for nowadays (document editing, password storage, even web chat clients) can be done locally on your machine.
In some sense, **running something on a server is only done when that thing was made for a server-client paradigm.**
This is why I'm not so favorable towards these distributed/"web3" crypto networks that would let all clients act as servers.
Things like using a local password manager and writing documents locally on your computers are no-nonsense basics that I would expect people to be doing.
***New self-hosters want to re-build the cage built by Google and Microsoft with their homeserver,***
without realizing you don't need any server to have a similar (or better) experience.

## Complexity of Server Software
This is where stuff gets spicier.
**The Matrix "Synapse" server software is bloated.**
Sure, Matrix-org has an upcoming "next-gen" homeserver named ["Dendrite"](https://github.com/matrix-org/dendrite) which is more efficient and written in Go,
but this doesn't solve the inherit issue that it takes a corporation to write a server software nowadays.
The Matrix protocol is very complex and unorthodox,
which means that independent developers looking to develop a server software for it are looking at a large amount of work and testing.
Also, the Matrix spec is constantly undergoing changes and new additions,
which means most homeservers will likely run the de-facto "official" server software (matrix-synapse) **which is a bloated, Python mess.**

## XMPP to the Rescue?
XMPP by contrast is another decentralized chat protocol that is far simpler to write for due to its straight-forward usage of XML files.
I know lots of prominent Linux YouTubers have promoted it as an alternative to Matrix,
so I'll make something perfectly clear: ***XMPP. Is. Not. Matrix.***
Matrix is a fundamentally different (and admittedly overly complex) protocol that has very different defaults than XMPP.
While both protocols are highly extensible,
I think it'd be unlikely to see XMPP reach the usability and feature list of something like Discord,
at least not outside of closed, single-server deployments.

In summary,
***XMPP is better than Matrix for those who are worried about the long-term sustainability and future of such a complex protocol (Matrix) and wish for a more "universal" and KISS-like standard (XMPP).***
