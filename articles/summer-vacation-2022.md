---
title: Summer Vacation 2022
author: Denshi
date: Mon, 04 Jul 2022 16:08:50 +0200
---

I'm on vacation for the **entirety of July this year.**
I will not be working on any videos the entire month,
although I may relapse and edit my website or work on some self-hosting project here and there.

## Fun things I'm doing on vacation
Since I'm on vacation near the coast,
I go to the beach every single day and get to enjoy a stereo-typically hot and sunny summer.
I unapologetically adore the heat and the sun,
not because they don't make me uncomfortable sometimes (we all know the pain of being in a hot car for too long, or being unable to sleep due to the heat)
but because the alternative, freezing myself, sounds far less appealing.
Also, it's a great excuse to eat ice-cream.

I am hard at work making the next [Sabaudia](https://sabaudia.xyz/) album,
and a rural vacation like this is a Godsend for creative energy.
I'm able to write music much more effectively now that I have less pressure in my life.

Finally, I've had the chance to use lots of **minimal personal websites** and **self-hosted services** which are made even more useful since I'm operating entirely using a **rural internet connection.**
It's a good thing I'm not using large, bloated web services!
I've been updating my personal website in general, too;
for example, I added new friends to my [friends](https://denshi.org/friends.html) page.

## Fun things I'll do when I'm back
I'm going to **finally make more Wide Web World episodes** alongside more long,
well-edited videos in general.
I know my audience will watch and enjoy the shorter,
recorded videos I make, but I get much more fulfillment out of my more thought-out videos.
Keep an eye out on YouTube and Odysee!

I also want to add more things to my [Self-hosting services page.](https://denshi.org/services.html)
For example, I want to get around to fixing my PeerTube instance since I haven't been able to find a server to host it on yet.
Once I do this, I will be sure to notify everyone with a blog post.

Anyways, just because I'm on vacation doesn't mean I'll be completely dead on the internet.
**If you need support or an account on my services (ie. Matrix) then feel free to email me.**
Besides that, I hope everyone has a nice summer. See y'all in August!
