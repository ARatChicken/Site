---
title: Added Features to the Blog System
author: Denshi
date: Sat, 25 Sep 2021 09:38:53 +0400
---

*Recently, I've been tinkering around with Luke Smith's 'lb' script and adding various useful features.*

The most useful one (which you will notice at the bottom of this entry) **is the new (Reply-to) link at the end of all articles.**
This is appended automatically by the script every time I publish a new article,
and it works on RSS as well!
This way, people have a **convenient way of contacting me about any article;**
The link also sets the subject of your email to the title of my article, for extra comfiness.

One minor addition is that I fixed the links in the RSS feed.
Now my articles link directly to their blog page counterparts,
instead of linking to the now-defunct rolling blog page.
(No one even used that anyways!)
