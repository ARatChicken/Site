---
title: New Band, New Album!
author: Denshi
date: Mon, 16 May 2022 20:05:00 +0400
---

Hi, I'm Denshi, the musician previously behind "DenshiSong".
I made a new band, "Sabaudia" and I've just released my first album, Wreck of the Zanzibar!
It contains a mix of influences, like math rock, a tiny bit of punk and some synthwave.
The songs here are genreless and I wasn't aiming for any specific sound;
this is just what came out when I forced myself into making an album with actual lyrics:

![Cover art to my album](https://sabaudia.xyz/zanzibar/cover.jpg)

In addition to Sabaudia's Bandcamp, you can also check out my band's website at sabaudia.xyz. The website has lots of cool things, such as:

 - [Lossless downloads for my albums](https://sabaudia.xyz/files/Releases)
 - [Demos and project files](https://sabaudia.xyz/files)
 - [A "behind the scenes" article](https://sabaudia.xyz/zanzibar/the-making.html) for Wreck of the Zanzibar
 - [A page with all the lyrics](https://sabaudia.xyz/zanzibar/lyrics.html)
 - And probably more coming soon, stay in touch!

The "behind the scenes" article is also available on my denshi.org RSS feed so make sure you don't miss it. "Wreck of the Zanzibar" is licensed under [CC BY-NC 3.0.](https://creativecommons.org/licenses/by-nc/3.0/) Feel free to share or adapt the music however you like!
