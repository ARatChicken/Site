---
title: YouTube Censorship and Live Streaming Updates
author: Denshi
date: Sun, 28 Aug 2022 09:30:06 +0400
---

Yep, it happened again! I got a YouTube community guidelines strike for "harmful and dangerous content".
Now what that really means is that I accidentally talked about youtube-dl in a [2-year-old video of mine.](https://odysee.com/@DenshiVideo:f/5-features-i-d-like-on-youtube)
If you want to know more about this situation, please watch my video about it on [Odysee.](https://odysee.com/@DenshiVideo:f/YouTube-Censorship-Returns!-(+Live-Stream-Updates):a)

## DenshiLive!
After a little bit of tinkering with some streaming server software,
**I finally figured out how to stream to my own self-hosted website!**
While I'm not streaming at the moment, you can head on over to [denshi.live](https://denshi.live) and subscribe to its [RSS feed](https://denshi.live/rss.xml) to receive updates on when I'm going to stream.
**You can also watch the stream from your own media player, like MPV or VLC Media Player.**
