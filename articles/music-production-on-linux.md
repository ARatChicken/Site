---
title: Music production on Linux
autor: Denshi
date: Fri, 24 Sep 2021 21:24:12 +0400
---

*For a very, very long time,* musicians all over the world have employed the use of computers to aid them in music production.
**GNU/Linux,** being the OS of choice for tinkerers and the technically inclined,
gained a major following in the music and video production scene for it's speed, reliability and compatibility over the years.
But what does music production on Linux actually look like?

## LMMS
[LMMS](https://lmms.io), also known as the Linux Multimedia Suite (or more commonly as "Let's Make Music") is a Digital Audio Workstation (DAW) for Linux, Windows and MacOS.
It's the single music production software I'm most experienced with,
so I'm going to list some pros and cons:

**Pros:**

 - It's great for MIDI and sequencing.
 - It's compatible with most LADSPA plugins and supports WINE for Windows VSTs.
 - It has one of the most intuitive interfaces in the world of DAWs.

**Cons:**

 - Recording isn't supported; you have to import audio.
 - It's just awful for mixing in general, the interface is not designed for it.
 - Plugin compatibility and support isn't as great as it should be.

## Ardour
[Ardour](https://ardour.org/) is a professional-grade (but still completely FOSS) DAW for Linux, Windows and MacOS.
**However, there's a catch:** unless you're on Linux, where your package maintainers already offer Ardour free to download, **you'll have to pay for binary builds of Ardour.**
I haven't heard of anyone on Windows or MacOS successfully compiling Ardour from its freely available source code,
so this one's basically only free for Linux users (unless you want to pay for binary Windows and MacOS builds).

I won't go through and list pros and cons like I did for LMMS, because Ardour only really has two big cons: firstly, dealing with MIDI can be awful.
Most people who do anything complex with Ardour utilizing MIDI will tell you about the various bugs and inconsistencies present in the software.
These aren't enough for me to recommend LMMS over Ardour,
however if your music is heavy on sequencing, you might want to consider using LMMS just to make the MIDI sections.
The second big con is the learning curve.
This is a minor con, as all audio software is designed by alien creatures with no understanding of intuitive design,
but Ardour can be especially weird and specific in its UI. Just something to consider.

## Proprietary DAWs (eg. Reaper)
While there are various proprietary DAWs available for Linux, **I don't recommend any of them.**
Not only are the proprietary, closed (and sometimes even paid) software,
but they're never as fast, stable and supported as their FOSS counterparts.

With that said, I have used Reaper before, and I didn't find it to be entirely awful.
It works, but I'd recommend using Ardour over it.

## Conclusion
In conclusion, while LMMS is a great learning and sequencing tool for music,    
anyone intending to do any serious music production on Linux should look into Ardour.
It can be hard to learn, but like previously mentioned, DAWs are designed by aliens.
*They're going to be difficult to use!*
