---
title: Stop Posting About Me on 4chan
author: Denshi
date: Fri, 14 Oct 2022 05:24:15 +0400
draft: false
---

It has been brought to my attention by my friend [webb](https://spiderden.net/)
that someone has been [posting](https://boards.4channel.org/g/thread/89187139)
about [me](https://boards.4channel.org/g/thread/89187863)
to the 4chan imageboard /g/.
Just to clarify, ***this is not me.*** I wouldn't dream of self-promoting on 4chan.

That statement aside, I'd like to take a minute to actually talk about 4chan
and give y'all the usual lecture about how all social media is bad.
I've done it before with the [Fediverse](/blog/dont-put-up-bars.html),
when I talked about how it not only allows for centralized censorship,
but more generally, ***rots your brain.***

## "B-but they're better than Reddit!"

Some may argue that "imageboards",
social media systems that rely on inflammation to rank posts
are a better alternative to voting-based websites like Twitter, Instagram or Reddit.
Yeah! In the same way, **nicotine gum** is a "better" alternative to **smoking nicotine**;
***it's much better not to take any nicotine in the first place.***
Face the facts: People are wasting unreasonable amounts of their:

 - Time
 - Energy
 - Effort

For websites and systems that ***do not benefit them whatsoever.***
In fact, in many ways websites like Instagram and Reddit have an upper hand to 4chan
simply because they're more popular with people,
meaning their users may be somewhat inspired to spend some time out of the app
and with their friends who also (likely) use the very same app.
The "best" parts of 4chan are always those which leak into other social medias
and spread to real life experiences;
essentially, the more actually ***social*** the website is.

## Free Access to Explicit, Illegal Content

Also, here's a friendly reminder that [explicit content is illegal in most nations](https://denshi.org/antiporn).
4chan is notorious for its very crude culture and userbase,
and the website offers ***free, unfiltered access to explicit content***
to all of its users.
The little time I've spent on /g/ (the 4chan imageboard themed around technology)
also indicates that entirely unrelated conversation
must be plastered with images of "anime catgirls"
and other such heinous, inappropriate suggestive imagery.
It might be the fact that a large proportion of the userbase is composed of men
(and young men at that)
but it's ***still completely unacceptable for this to be "normal"***
and I implore all users of this website to stop using
to prevent the spread of this behavior.

## Memes and /b/

Here's a story some of you might know:
Long ago, before time had a name, the 4chan "random" imageboard (/b/) was king of all memes online.
It was where ***everything*** was happening;
all the drama, all the funny images, and all the useless fighting with 9gag.
**That was 10 years ago. Wake up.**
The internet "golden age" is long over,
and now it's up to you to stop going to these dead websites.

