---
title: Autism Awareness Day?
author: Denshi
date: Mon, 05 Apr 2021 00:09:10 +0400
---

We all know how much people love dedicating days to pointless things.
From international pancake day to "national step in a puddle day",
there's no shortage of random holidays human beings have devised.

However, we are meant to take these days more seriously when there is some supposed valued "cause" or "movement" behind the holiday itself.
Just to clarify I am not speaking of religious or historical celebrations;
these are not what I'm addressing in this post.
I'm instead speaking of days akin to "Autism awareness day".

## Autism people as a crutch
Let's cut to the chase:
we all know that Autism, just as race and sex, is what politicians and people on the internet are going to begin using as a crutch to gain victim points.
It's the next in a long line of victim cards which will be used against anyone who dares speak against what the owner of said card believes in.
And as an individual with autism, I find this very very upsetting.

The reasoning behind "Autism awareness day" is supposedly to make people aware about the existence and nature of individuals on the autistic spectrum.
I guess it's called "awareness" because you can't always tell someone is mentally disabled just by looking at them,
unlike you could with other minorities such as racial minorities,
or other victimized groups such as women.

## "Awareness" only makes sense for Twitter users
While the idea of making people aware of autism is noble in some sense,
I think anyone with any shred of critical thought can instantly devise it's simply yet another way for pointless labeling and tribalistic attitudes to propagate in the world.
Making people aware of autism's existence subconsciously tells people without it that they are different to people with it;
this feeling of difference from a natural, genetic standpoint is what deplorable websites like Twitter.com prey on:
Identity politics and tribalist attitudes are widespread as everyone is out there to prove they're either the most oppressed or the person who is most in touch with the supposedly oppressed.

It's perfectly fair to say that attitudes on Twitter do nothing to help those who are truly in need of this sought-after "equality".
Twitter represents the walled-off rich suburbs in California,
or the hipster coffee shops in New York City:
Completely and utterly out of touch, yet supposedly "on the side of the people" or at least the "minorities" when in truth they're probably the most destructive thing for any group they claim to be helping.
Autism is simply yet another "oppressed" group for these people to prey upon,
and these international awareness days are their opportunities to whine and pout about how sensitive they are and how much they tolerate a people group(autistic people) when this couldn't be further from the truth:

## My conspiracy theory
I personally believe the day will come when something akin to "Black Lives Matter" will happen with autistic people:
A violent or at least highly vocal movement that claims to be on the side of the oppressed,
but does much more damage than actually helps.
However, this will only happen once all the autistic people who yet are not victims are either censored or eliminated.
A prime example is 4Chan: It's undeniable that lots of the website if not most of it is populated by people on the autism spectrum.
Autism affects the reasoning of the brain, something other victimized groups don't really experience,
which means that autism has some inherit mental features.
I have reason to believe the nature of autism makes people attracted to political extremism or at least some political polarization,
both to the left and to the right.
I've often seen more high-functioning autistic people be on the right,
but I don't have any empirical data.

## Autistic people are immune to propaganda
4Chan is mostly populated by people with right-wing or at least politically incorrect opinions,
the sorts that make users of Twitter and other politically correct social medias seethe with rage.
It's pretty funny honestly.
But if we are to believe most of these people are autistic,
then I don't think an "Autistic Lives Matter"-esque movement could ever happen without the censoring of all these politically incorrect autistic people.
In a sense, autism is far more difficult to contain and use as a victim group for empathy points,
at least when compared to something like race or sex.
