---
title: I Finally Made a Full Album!
author: Denshi
date: Fri, 26 Aug 2022 12:50:40 +0400
---

I've mentioned my band, [Sabaudia,](https://sabaudia.xyz) previously on my personal blog. I make alternative music with my siblings and publish it to both the band's [own site](https://sabaudia.xyz) and [Bandcamp.](https://sabaudia.bandcamp.com) I'm writing today to inform my readers that after months of work, my first ever full album, [Orioness Red](https://sabaudia.xyz/orioness/orioness.html) is available for free download on the band's site.

## So, what's it about?

The album is genre-less, but unlike my previous EP (Wreck of the Zanzibar) it's a ***concept album,*** meaning it has a beginning, a middle and an end; a story. There are plenty of songs in there (13+1 tracks) so I'm sure you'll find at least one thing you like if you take a listen!
