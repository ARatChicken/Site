---
title: The Social Consumer
author: Denshi
date: Sun, 16 Oct 2022 10:24:58 +0400
draft: false
---

For anyone who keeps up with my channel and site,
you'd likely already be aware of the **DenshiVideo chats.**
These **were** group chats available on *Matrix, XMPP* and *Discord*
that let users chat across them seamlessly.
Now if you also keep up with the happenings in online group chats (I'm sorry for you)
then you'll also be aware that **they were closed quite recently.** Why is this?

## What Happened?

When I first created the **DenshiDiscord** in 2020,
my initial aim was to create an online chat that could help provide and build resources
to help people use computers,
with the added benefit of being able to engage with my fans.
This best manifested in the form of the **tech support** channel,
which was actually quite successful at helping strangers with tech problems.

However, the cracks in the system were there from day 1.
The original Denshi chats were created on **Discord,**
a messaging service known for its especially anti-social users.
Even my wholesome Catholic technology channel wasn't safe from the claws of Discord users.

**Ok, but why?**
Why is it that people on messaging programs like **Discord**
or services that offer messaging like **Twitter**
**consume messaging** as if it were some kind of **social media?**
They reload their proprietary messaging programs and feeds many times a day,
waiting for *someone* to respond. Anyone!
Just like a heroin addict longing for someone to plunge the "social" needle into their vein.
**How did this even happen?**

## Computers for Humans

Like the citizens of Oceania in *Nineteen-Eighty Four,*
we have forgotten the past and prefer to remain ignorant of how things used to be.
Let's take a look at what online communication entailed **before** the rise of Discord:

![Zoomers will never understand.](/mumble.png)

I remember being what some would consider a *"gamer"* when I was younger,
and if you know anything about **epic gamers,**
it's that **voice calling** is an essential part of online play (Playing Flight Sim X with ATC!).
Around 2015-2017, the best way to accomplish this
was to use a service such as [Mumble](https://mumble.info), Skype or TeamSpeak.
*Replacing these "antiquated" systems was the original stated purpose of Discord.*

**Notice how Skype, TeamSpeak and Mumble are not "social" in nature.**
What I mean by this is that barring any "server discovery" features in Mumble,
you're most likely using Mumble to speak to people **you already know,**
whether through your daily life, or online activity elsewhere.
**This technology was meant for and used by humans.**

**The chat user of today however, is different.**
I've seen the transformation first-hand.
I remember people telling me to get Discord at school,
where we used it to communicate in my friend group,
just like gamers used to use programs like Mumble.
But over time,
I started to notice a serious shift in the usage of the program.
**Messengers like Discord were no longer means to an end,
but entirely new ways to socialize.**

## Average Social Consoomer

This brings me perfectly to the topic of **The Social Consumer.**
Being one is a horrible existence, 
characterized with wasting eons in front of a computer screen,
waiting for "replies" rather than just moving on with your life.
I should know, I was once one of the worst.

The rise of the internet as a new paradigm for interaction
as opposed to a means to an end
has not gone unnoticed, however:
There are many who seek to exploit the parasocial tendencies rampant among teens,
in the form of an **unending stream of "drama"**
to entertain the innocuous masses.
This has to be the most concerning aspect for me,
as a video maker.

I've seen far too many people get entranced with stories and current events
that would otherwise have *absolutely 0 impact* on their "real" lives whatsoever,
**yet another instance of "social" consumerism.**
And it's not mere innocent entertainment:
Ask yourself, is it worth to be "entertained"
if entertainment demands such a high degree of engagement
for you to "keep up" with anything that's going on?

Remember that this all stems from the fundamental idea that
**communication is entertainment,** and not the other way around.
These poor users continue to fire up their messengers on the daily,
seeking that ever-so-valuable attention from others,
just as a more traditional consumer wastes away his earnings
on little plastic figures,
hoping for validation.

## Not Just Discord

This same "social messenger" mentality began to bleed **everywhere.**
This includes genuinely good chat protocols,
such as [XMPP](https://xmpp.org) and [Matrix](https://matrix.org).
People were now creating Matrix *"Spaces"*,
collections of chats extremely similar to Discord servers.

Entire online federated systems like **Fediverse** were built around
**re-creating the anti-social structures of Twitter microblogging**
with a fresh, decentralized coat of paint.
Fediverse users have the same issues with so-called "drama"
confirming my theories that **the microblogging social media paradigm is fundamentally flawed.**

There are many who will "bully" users of specific programs or platforms
because of the admittedly true tendencies that users of these platforms tend to have.
**Never forget that it's the user to blame for this behavior,
and the website or program can only aid it as much as they are willing to go through with it.**
If you want to waste 5 hours a day
scrolling through Discord "servers"
then this isn't much different from doing the same thing
with Matrix "Spaces".
The only reason we don't hear about teenage Matrix users starting drama
is because **the protocol doesn't have the userbase for these behaviors to be widespread yet.**
Regardless, in both cases the user is to blame, at the end of the day.

## Conclusion
Concluding this botch-job of an article explaining why I closed a chat server,
**"The Social Consumer" is real and a seriously bad part of people's personalities.**
Please do your part by purging these destructive behaviors if you can,
and **stop asking me to make a new chatserver so you can repeat them.**
Oh and, make sure to use Mumble. Mumble is pretty cool.
