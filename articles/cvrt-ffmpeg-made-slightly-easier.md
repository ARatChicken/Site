---
title: cvrt: FFMPEG, Made Slightly Easier
author: Denshi
date: Mon, 22 Nov 2021 08:29:06 +0400
---

If you've been using Linux for a few days now,
you're probably already aware of the swiss-army knife of media conversion and compression: [FFMPEG.](https://ffmpeg.org/)
It's often pre-installed on most Linux distros,
and it offers a simple command-line way of converting between all (most) media formats.

*However, despite its compatibility and ubiquity,*
I've always felt FFMPEG doesn't do enough to handle repetitive,
batch-like conversions using regular expressions.
For example: imagine you had a folder full of mp4 files,
and you wanted to convert each of them to mov. With cvrt, you can run the following command:

```
cvrt *.mp4 mov
```

All this script does is take two arguments: the input files (\*.mp4),
and the last argument passed, which is the output format/file (mov).
Another useful feature of cvrt is the fact you can specify cases for outputs.
For example: if I wanted each mov file to be in a specific audio and video codec (ie. for DaVinci Resolve), then you can specify it in the script:

```
case "$ext" in
    *mov)   OPT="-c:v mpeg4 -q:v 0 -pix_fmt yuv420p -c:a pcm_s16le $OPT" ;;
esac
```

In this case, there is a case for all **.mov** output files, to ensure the resulting files always come out with specific codecs, using FFMPEG flags.

The script can also **concatenate files!** This means you can run:

```
cvrt file1.mp4 file2.mp4 output.mp4
```

This way, file1.mp4 and file2.mp4 will be concatenated into a single video, output.mp4.

## [Get cvrt Now](https://codeberg.org/Denshi/Scripts/src/branch/master/cvrt)
