---
title: Guide to GCSE and A-Level
author: Denshi
date: Sun, 03 Oct 2021 16:53:27 +0400
---

Whenever I talk to my younger siblings and their friends about their future in school,
I often see a **lot of confusion regarding the current educational and examination systems.**
In this article, I will list and summarize the most common British qualifications,
alongside some editorial paragraphs giving my opinion on each one.

## (I)GCSE
 - Length: 2-3 years
 - Grades: U-9 (U-A\*\*)
 - Years: 9-10/11

### Summary
The **(International) General Certificate of Secondary Education** is a qualification standard introduced in the UK in 1988.
(The "International" GCSE is essentially the same system, but with some content cut for international audiences.)
The (I)GCSE has a set of core subjects which are mandatory to complete;
one of these is Humanities, for which you can pick between History or Geography.
Alongside this, many schools will have 2-3 more mandatory subjects that you can pick from the vast (I)GCSE options.

### Note on Languages
The most common languages schools offer will be **Spanish, French, German** and **Mandarin.**
However, there are GCSE qualifications for many, many other languages,
such as **Italian, Modern Greek, Japanese, Polish, Portuguese** and a few more.
*Schools may offer these as extra-curricular activities rather than in the form of school day lessons.*
For example, I got an Italian GCSE qualification by doing classes after school and taking the exam in the school's examination facility.

### Further Mathematics
The (I)GCSE Mathematics course can optionally be squeezed into 2 years to allow students to complete the **Further Mathematics** course in the last year of (I)GCSE.
This course can be **very intensive** but it does prepare students really well for A-Level and even IB Math courses, so only pick this option if you really love Maths!

### Triple and Double Science
Some schools offering the (I)GCSE Science courses may let you qualify with a **triple** or a **double science award.**
A double science award (Contrary to its name) still has you study all 3 sciences,
**however some content is cut from the syllabus.**
This extra content is only covered by students completing the triple award.
Think of the double award as an intermediate science course,
and the triple award as an advanced science course.

### Core Subjects (Mandatory)
 - English
 - Mathematics
 - Science (Physics, Chemistry and Biology)
 - Language
 - History and/or Geography

### Commonly Available Option Subjects (2-3 are often mandatory):

#### Sciences:
 - Statistics
 - Computer Science
 - Psychology
 - Economics
 - Business Studies
 - Physical Education (Poor man's Biology)

#### D&T:
 - Food and Nutrition
 - Design and Technology
 - Engineering

#### Art:
 - Music
 - Art and Design
 - Dance
 - Drama
 - Photography

### Resources
For (I)GCSE Mathematics and Further Mathematics, **I highly recommend any of the Casio FX-9X series of non-graphing calculators.**
Their build quality is excellent, they are solar-powered and can do pretty much everything you need for all (I)GCSE maths courses.

### Recommendations
The subjects I took for my (I)GCSEs are *English, Mathematics, Further Mathematics, Biology, Chemistry, Physics, Statistics, Economics, Computer Science and French.* I also completed the aforementioned Italian GCSE. The only subject out of these that I strongly recommend to students is **GCSE Statistics.** Statistics is extremely useful if you plan to pursue any remotely scientific subject. It is quintessential to the study of **Psychology, the 3 sciences,** and the **Statistics course in A-Level Mathematics.**

## (I)A-Level
 - Length: 2 years
 - Grades: U-A\* (Only U-A for AS-Levels)
 - Years: 12-13

The **(International) Advanced Level** is another UK qualification like the GCSE,
except it's what's known as a Level 3 qualification.
This means it's the course Universities and Colleges are concerned about when they consider candidates.
It was introduced **all the way back in 1951,**
making it far more established than the younger GCSE system.
Because of this longevity, the A-Levels vary all across the world,
with various countries (Such as Hong Kong, Singapore and Mauritius) including A-levels as part of their high-school diplomas.

### The Magic of A-Level
Unlike (I)GCSE, which has a mandatory set of "core" subjects, **A-Level has no course requirements.**
They are purely qualifications in any field you like.
This means most schools will often require students to complete 3-4 A-Levels of their choosing in any subjects, giving the student complete free reign on the fields they wish to explore.

This unrivaled level of flexibility in terms of subject choice lends itself well to the intensity of the courses,
meaning that any A-Levels you choose should be for **subjects you (really) like!**
If you're going to be doing only 3-4 subjects for 2 years, they better be good ones.

### AS-Levels
While the overall qualification system is called "A-Level",
the qualifications themselves are split into 2 categories: **A-Levels and AS-Levels.**
A-Levels are 2-year courses that are graded from a U (insufficient) to A\* (distinction), while AS-Levels only last a year and are graded U to A.
*Some schools will offer you 3 A-Levels and 1 AS-Level course,*
meaning you only study the AS-Level subject for a year.
While every A-Level is a 2-year course,
**the AS-Levels are just the first year of content, and then a test.**

### Modular and Linear Courses
Depending on your school and exam board, you may be offered **modular A-Levels.**
These often come in the form of the sciences, such as modular Physics or Biology.
All modular means is that you will be **taking exams on each individual subject module,**
instead of on all the course content at the end of the year.
So if you study module 1 for Biology, you will sit a test for that module,
and then move on with your life.
This is opposed to sitting a test with that content and sitting on it for a year until the final exam.

*However, please do note that some schools and exam boards are transitioning towards linear A-Levels, meaning modular courses may be on their way out soon.*

### Commonly Available Subjects:

#### Languages
 - French
 - Spanish
 - German
 - Italian
 - Japanese
#### Sciences
 - Statistics
 - Physics
 - Biology
 - Chemistry
 - Mathematics
 - Further Mathematics
 - Computer Science
 - Psychology
 - Geography
 - History
 - Business
 - Economics
#### Art
 - English
 - Art and Design
 - Drama
 - Music
 - Dance
 - Photography
#### D&T
 - Design and Technology
 - Food Technology

### Resources
If you're doing **A-Level Mathematics,** then the calculator I talked about in the GCSE resources section (Any Casio FX-9X series scientific calculator) should be fine.
However, if you're doing **A-Level Further Mathematics,** you may be interested in purchasing a more advanced graphing calculator such as the Casio PRIZM FX-CG50. While expensive at upwards of 80$, these are still a far better deal than the [atrocious Texas Instruments calculators.](https://www.youtube.com/watch?v=J2kvl3YnA2w)

### Recommendations (Not)
I'd be remiss if I gave recommendations about the A-Levels to choose.
The whole point of the A-Levels is to give the power and flexibility to the student.
With that said however,
I do recommend you do any and all subjects required (or recommended) for a specific university or college course you wish to apply to.
