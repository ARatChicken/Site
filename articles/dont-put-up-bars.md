---
title: Don't Put Up Bars.
author: Denshi
date: Tue, 18 Jan 2022 08:44:25 +0400
---

***Computer development and its consequences have been a disaster for the human race.***
You'd probably find this difficult to believe, especially coming from an avid l33t LUNIX h4xxor like me,
but more and more people are waking up to this important truth.
It only takes a short browse through Twitter,
a couple of posts from 4chan and a brief look at Facebook's practices to realize that we went wrong.

**But where exactly did we go wrong?**
Here, some people will point towards the ideal of "free software";
software that respects the end user's freedom.
*"We went wrong when we allowed for software to be published,
sold and distributed without meeting the adequate transparency and legal requirements to do so;
Making the source code completely public and allowing for permissive licensing (ie. GPL, MIT...)
is the only way to restore software to its former glory."*
**Now this is very good, and a positive development and all,**
however one must realize that making software with the intent of making it as freedom-respecting and transparent as possible *doesn't guarantee that it's going to positively benefit society.*

## Twitter VS the Fediverse

Take the Fediverse, for example. Fediverse protocols (such as ActivityPub) are often used to create Twitter clones, like Pleroma and Mastodon.
**Unsurprisingly, these places tend to be just as inappropriate,
filthy and brain-rotting as Twitter itself.**
This is despite the Fediverse being designed to be free and open source!
So maybe "free software" itself isn't as important as a computing philosophy that **includes free software!**

## Muh bloated DEs

Another, more subtle example are big desktop environments like **GNOME and KDE.**
*Don't get me wrong,* I love KDE and GNOME, and their software is perfectly freedom-respecting,
but I think I wouldn't be the first person to point out that they keep trying to be like their proprietary alternatives.
GNOME's user interface especially seems to get more and more similar to MacOS with every update,
and the releases keep getting more and more bloated.
**There is an unhealthy obsession with making things "user friendly"; what they mean by this, is "windows/mac friendly".**
And while there's nothing wrong with designing software that's easier to use,
(it's very beneficial actually) there clearly is something wrong when an obsession with support and user-friendliness leads to poor programming practices, security issues and bloat.

A side-note: A lot of these "FOSS companies" signed the [anti-Richard Stallman letter.](https://rms-open-letter.github.io/)
**Look, I don't admire Stallman's political views. Like at all.**
But he quite literally founded the modern free software movement, and is the perhaps the physical embodiment of what software freedom is all about.
*If any of these organizations actually cared about free software,
they wouldn't sign a document like this.*

## "Free Software" is meant to liberate you.

At the end of the day, free software is only as good as it liberates you; not only from the restrictions of proprietary companies and licenses,
but also from the **detrimental habits that these companies are incentivized to spread.**
 - It's in Twitter's best interest to keep you scrolling
 - It's in Instagram's best interest to ruin your self-esteem
 - It's in Microsoft's best interest to keep you using bloated software, so you'll buy more powerful (more expensive) hardware from the manufacturers they're partnered with

These practices and habits are certainly enforced by the proprietary and closed nature of software,
but one must also stop to consider how these attitudes can seep into the open source world as well.
**A license can never save you from the stupidity of certain people.**

