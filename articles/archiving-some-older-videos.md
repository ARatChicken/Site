---
title: Archiving Some Older Videos
author: Denshi
date: Sat, 16 Oct 2021 17:33:26 +0400
---

Sometimes I take down videos from my channel.
It's normally for a combination of reasons,
but what it really waters down to is that **I don't think those videos leave an accurate impression to new viewers.**
Here's a list of reasons I may want to take down a video:

 - It's incredibly poorly-made/There's some giant error
 - It doesn't fit the the general "theme" or tone of my channel
 - It seems backwards and harmful in hindsight

Every time I take down a video, I instantly upload it to my backup channel, [DenshiBackup](https://www.youtube.com/channel/UCwZSxhxlFnuLzk-UkujcDWg).
This is because while **I don't want to make my videos lost media!**
I'm sure that basically no one cares much for any of my videos, especially my older ones,
but they have sentimental value to me (yes, even the garbage ones) so I keep them there for myself and anyone else who is interested.

## This time however, it's a little different
I know that most people reading this blog are **unaware of my time spent as the editor** for the [MLAATR Fanbase](https://www.youtube.com/c/MyLifeasaTeenageRobotFanbase) YouTube channel.
I only ever edited [4 videos for them,](https://www.youtube.com/playlist?list=PLBdDbs7--MKtgfJ0s97_ixZ_djI8ObwX9) but after editing the second video for them,
I joined their **Discord server.**
I won't bother recalling the story of what happened there, because there's already a great article on the DenshiWiki.

## We Do a Little Trolling
Long story short, there were quite a few grievances (and a lot of autistic behavior on my part) relating to the MLAATR Fanbase and its members/leadership.
I was banned 3 times from that chat server,
and the controversy would inspire me and The Official Uno (and a few other people,
like Rabbit and Yeokii) to embark on a trolling journey.
**We made an alt account named "NebUla";
She joined the server, impersonated by Uno, and chaos ensued in the coming days.**

To document the happenings, Uno recorded and edited a video with my help named "𝐌𝐲 𝐥𝐢𝐟𝐞 𝐢𝐧 𝐭𝐡𝐞 𝙈𝙇𝘼𝘼𝙏𝙍 𝔻𝕚𝕤𝕔𝕠𝕣𝕕 (Feat. TheOfficialUno and Yeokii)".
***I'm ashamed of this video.***
Not because of its writing, production quality or editing (Uno did the editing, and he actually knows what he's doing),
but because of a few factors that make it very embarrassing:

### The "Silent interview"
For a **very large portion of the video,** I interview someone named SilentDrift.
This interview is the most commonly criticized aspect of the video, and it's the part I was considering cutting before uploading it.
With time, my grief regarding this interview has only grown;
I asked overly provocative questions,
I asked for unnecessary details and incentivized Silent to give anti-fanbase answers **because I was an autistic little idiot.**

### NebUla as a whole
I won't deny that NebUla was fun. Uno, Rabbit, even Yeokii would admit this;
the memes were fun to make, it was fun to mess with people. But it was really stupid too.
To be honest, the most impressive part of the video is the "social engineering" we did to get around the MLAATR Fanbase's sophisticated anti-alt measures (Checking whether you have an account on other platforms).
It wasn't hard or impressive at all really, **but at least it was something.**
Overall, it was kind of a waste of time.

### It wasn't all bad!
The conclusion is beautiful. Uno's narration and the script are beautiful.
Constructive criticism is given to the fanbase,
and it's a non-aggressive end to what was an awful video.
So I'm still proud of Uno for pulling that off.

### I'm taking the video down.
After some consideration, and some time spent rethinking my channel and its videos, =
I've decided to take the video down.
I'm [archiving all the comments on Archive.org,](https://archive.org/details/DenshiMLAATRComments) and the video is [archived on DenshiBackup.](https://www.youtube.com/watch?v=S87wBXgcxBE)

## Other things I've taken down

***Besides the MLAATR video, I archived a few older playlists from my channel onto DenshiBackup. Here's a comprehensive list of them, giving a short summary for each one.***

### Zoomer Rants in Home
This was a rant series, with a title inspired from Luke Smith's "Boomer Rants in Woods" playlist.
Despite the title however, few of these videos were actually made in my house;
Many of them I recorded in my garage, in the garden or walking outside. I used to record a whole lot with my phone back then,
so it only made sense to me to record some thoughts and rants from time to time.
[This series is archived on DenshiBackup.](https://www.youtube.com/playlist?list=PLBdDbs7--MKtlrGjhx5oRKE5-sJPnNpfT)

### Musical Mondays
Feeling quite *musically inspired and re-invigorated* by the later months of 2020,
I started a series where I covered a new song I made every Monday.
This lasted all of 5 days until I gave up.
[This series is archived on DenshiBackup.](https://www.youtube.com/playlist?list=PLBdDbs7--MKvqBpCRvS16xEsgKkppKtzk)

### DenshiDays
Right when the COVID pandemic hit, and our school closed down for two weeks,
I dared myself to make a video everyday for a week straight.
This manifested itself in the form of a **garbage compilation of ultimate fail.**
I've archived the whole series for your viewing pleasure,
and taken down the original videos from my channel.
[This series is archived on DenshiBackup.](https://www.youtube.com/playlist?list=PLBdDbs7--MKsuC3QIWq1YCb_MpdWabjLN)

### "Just play" Games
"Just Play" Games was a spin on the "Just Use" Software series, but with games instead of software.
As you'd expect, *this died very, very quickly* just like my interest in video games.
[This series is archived on DenshiBackup.](https://www.youtube.com/playlist?list=PLBdDbs7--MKuDWPYrzgrrmIcLV003ZPX3)

## Conclusion
**I don't like taking videos down.** I really don't.
But I think for the long-term sustainability of my channel (and my web presence in general),
it's important to manage what people see when they come to my channel.
The Zoomer Rant Videos, the Musical Mondays,
and the MLAATR Fanbase video are all things that don't represent who I am anymore, but more importantly,
they leave an inaccurate impression on new viewers about what my channel is about.
**My channel is not about bullying, rants or trolling.
It's about technology, learning, humor and internet history.**


